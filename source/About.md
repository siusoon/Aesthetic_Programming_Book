Title: Biographies
page_order: -4
status: hidden

**Winnie Soon** was born and raised in Hong Kong, increasingly aware of, and confronting, identity politics regarding its colonial legacy and postcolonial authoritarianism. As an artist-coder-researcher, she/they is interested in queering the intersections of technical and artistic practices as a feminist praxis, with works appearing in museums, galleries, festivals, distributed networks, papers and books. Researching in the areas of software studies and computational practices, she/they is currently Assistant Professor at Aarhus University, Denmark.

**Geoff Cox** likes not to think of himself as an old white man from a parochial island but is clearly in denial. Thankfully other aspects of his identity are more ambiguous and fluid. Research interests lie broadly across the fields of software studies, contemporary art practice, cultural theory, feminist new materialism, and image politics, reflected in his academic position as Associate Professor and co-Director of the Centre for the Study of the Networked Image at London South Bank University, UK, and as visiting academic at Aarhus University, Denmark.

